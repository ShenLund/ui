$( document ).ready(function() {
    // var spaceAPI = "https://bitbucket.org/ShenLund/ui/raw/59f572e275ef43210b9554c132e264fa766e13fa/resources/space-api.raml";
    var spaceAPI = "https://bitbucket.org/ShenLund/ui/raw/fc1d569ed4198a6b947f70ba312a966e66f6bd7f/resources/space-api.json";


    callAPI(spaceAPI);
});


function callAPI(spaceAPI) {
    $.ajax({
        url: spaceAPI,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
            console.log(data.space);
        },
        error: function() {
            console.log("There was an error");
        }
    });
}